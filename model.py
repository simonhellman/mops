import tensorflow as tf
import numpy as np
import pandas as pd
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, LSTM, BatchNormalization, Embedding, TimeDistributed, Conv1D, MaxPooling1D, Flatten
from tensorflow.keras.callbacks import TensorBoard, EarlyStopping
from kerastuner.tuners import RandomSearch
from kerastuner.engine.hyperparameters import HyperParameters
import time
from data_preprocessing import preprocess
from sklearn import preprocessing, metrics
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from xgboost import XGBClassifier
import xgboost as xgb
import matplotlib.pyplot as plt
import seaborn as sns
from earlystop import EarlyStoppingValAUPR

config = tf.compat.v1.ConfigProto(gpu_options = 
                                 tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8))
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)
tf.compat.v1.keras.backend.set_session(session)

"""Train and return a LSTM-based model"""
def LSTM_model(X_train, y_train, X_val, y_val, 
               epochs=10, batch_size=64, learning_rate=0.001, 
               decay=1e-6, binary_threshold=0.2):


    EPOCHS = epochs
    BATCH_SIZE = batch_size
    NAME = f"LSTM-type1-simple-{int(time.time())}"

    model = Sequential()

    #CNN
    #model.add(Conv1D(filters=5, kernel_size=3, activation='relu', padding='same', batch_input_shape=(BATCH_SIZE, X_train.shape[-2], X_train.shape[-1])))
    #model.add(MaxPooling1D(2))
    #model.add(TimeDistributed(Flatten()))

    model.add(LSTM(32, stateful=True , return_sequences=True, kernel_initializer="glorot_normal", batch_input_shape=(BATCH_SIZE, X_train.shape[-2], X_train.shape[-1])))
    model.add(BatchNormalization())
    model.add(Dropout(0.1))
        
    model.add(LSTM(16, stateful=True , return_sequences=False, kernel_initializer="glorot_normal"))
    model.add(BatchNormalization())
    model.add(Dropout(0.1))
    
    model.add(Dense(8, activation="relu", kernel_initializer="glorot_normal"))
    model.add(Dropout(0.1))
    model.add(BatchNormalization())
    
    model.add(Dense(8, activation="relu", kernel_initializer="glorot_normal"))
    model.add(Dropout(0.1))
    model.add(BatchNormalization())

    model.add((Dense(y_train.shape[1], activation="sigmoid", kernel_initializer="glorot_normal")))


    opt = tf.keras.optimizers.Adam(lr=learning_rate, decay=decay)

    model.compile(loss="binary_crossentropy",
                  optimizer=opt,
                  metrics=[tf.keras.metrics.AUC(
                                num_thresholds=50, curve='ROC', summation_method='interpolation', name="AUROC",
                                dtype=None, thresholds=None, multi_label=False, label_weights=None),
                           tf.keras.metrics.AUC(
                                num_thresholds=50, curve='PR', summation_method='interpolation', name="AUPR",
                                dtype=None, thresholds=None, multi_label=False, label_weights=None)
                          ])

    es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=20)
    #es = EarlyStoppingValAUPR(monitor='val_loss', value=0.91, verbose=1)
    tensorboard = TensorBoard(log_dir=f"logs\{NAME}", profile_batch = 100000000)

    #print(model.summary())
    
    remainder_train = X_train.shape[0]%BATCH_SIZE
    remainder_val = X_val.shape[0]%BATCH_SIZE
    
    history = model.fit(X_train[remainder_train:], y_train[remainder_train:], batch_size=BATCH_SIZE, epochs=EPOCHS, validation_data=(X_val[remainder_val:], y_val[remainder_val:]), callbacks=[tensorboard, es], verbose=2)
    
    return model

## Simple NN ####

"""Train and return a LSTM-based model"""
def Simple_NN(X_train, y_train, X_val, y_val, 
               epochs=10, batch_size=64, learning_rate=0.001, 
               decay=1e-6, binary_threshold=0.2):

    X_train = np.array(convert_data_to_rf(X_train))
    X_val = np.array(convert_data_to_rf(X_val))
    
    
    EPOCHS = epochs
    BATCH_SIZE = batch_size
    NAME = f"LSTM-type1-simple-{int(time.time())}"

    model = Sequential()
    
    model.add(Dense(128, activation="relu", kernel_initializer="glorot_normal"))
    model.add(Dropout(0.1))
    model.add(BatchNormalization())
    
    model.add(Dense(64, activation="relu", kernel_initializer="glorot_normal"))
    model.add(Dropout(0.1))
    model.add(BatchNormalization())
    
    model.add(Dense(16, activation="relu", kernel_initializer="glorot_normal"))


    model.add((Dense(y_train.shape[1], activation="softmax")))


    opt = tf.keras.optimizers.Adam(lr=learning_rate, decay=decay)

    model.compile(loss="categorical_crossentropy",
                  optimizer=opt,
                  metrics=[tf.keras.metrics.AUC(
                                num_thresholds=50, curve='ROC', summation_method='interpolation', name="AUROC",
                                dtype=None, thresholds=None, multi_label=False, label_weights=None),
                           tf.keras.metrics.AUC(
                                num_thresholds=50, curve='PR', summation_method='interpolation', name="AUPR",
                                dtype=None, thresholds=None, multi_label=False, label_weights=None)
                          ])

    tensorboard = TensorBoard(log_dir=f"logs\{NAME}", profile_batch = 100000000)

    #print(model.summary())
    
    
    history = model.fit(X_train, y_train, batch_size=BATCH_SIZE, epochs=EPOCHS, validation_data=(X_val, y_val), callbacks=[tensorboard], verbose=2)
    
    return model



"""Get data and run the model"""
def run_model(lag, step, conflict, epochs, batch_size):
    X_train, y_train, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=lag, STEP=step, CONFLICT=conflict)
    
    
    EPOCHS = epochs
    BATCH_SIZE = batch_size
    
    model = LSTM_model(X_train, y_train, X_val, y_val, epochs=EPOCHS, batch_size=BATCH_SIZE)
    
    return model, X_test, y_test
    
    
    
"""Tune an LSTM-based model"""
def tuner_model(hp):


    model = Sequential()


    model.add(LSTM(hp.Int("input_units", min_value=8, max_value=128, step=16), stateful=True, batch_input_shape=(128, 12, 19), return_sequences=True))
    model.add(BatchNormalization())
    model.add(Dropout(0.1))

    for i in range(hp.Int("lstmlayers", min_value=0, max_value=3)):
        model.add(LSTM(hp.Int(f"lstmlayer_{i}_units ", min_value=8, max_value=64, step=16), stateful=True, return_sequences=True))
        model.add(BatchNormalization())
        model.add(Dropout(0.1))
    
    model.add(LSTM(hp.Int(f"last_lstm_units ", min_value=8, max_value=128, step=16), stateful=True, return_sequences=False))
    model.add(BatchNormalization())
    model.add(Dropout(0.1))
    
    for i in range(hp.Int("denselayers", min_value=0, max_value=3)):
        model.add(Dense(hp.Int(f"denselayer_{i}_units ", min_value=8, max_value=128, step=16), activation="relu"))
        model.add(BatchNormalization())
        model.add(Dropout(0.1))
        
    model.add((Dense(1, activation="sigmoid")))


    opt = tf.keras.optimizers.Adam(lr=0.001, decay=1e-6)

    model.compile(loss="binary_crossentropy",
                  optimizer=opt,
                  metrics=[tf.keras.metrics.BinaryAccuracy(threshold=0.2)])
    
    
    
    
    return model

######################################### NAIVE MODELS ###############################

#Guess value of last months dummy.
def last_month_model(x, conflict, scaler):
    x = scaler.inverse_transform(x[1:])
    if (x[conflict-1] > 0):
        return 1
    else:
        return 0
    
def run_last_month(X, y_val, conflict, scaler):
    y_val = y_val[:,-1]
    right = 0
    wrong = 0
    y_hat = []
    for idx, x in enumerate(X):
        pred = last_month_model(x[-1],conflict, scaler)
        y_hat.append(pred)
        if pred == int(y_val[idx]):
            right += 1
        else:
            wrong += 1
    
    y_hat = np.array(y_hat)
    log = metrics.log_loss(y_val, y_hat)
    roc = metrics.roc_auc_score(y_val, y_hat)
    pr = metrics.average_precision_score(y_val,y_hat)
    brier = metrics.brier_score_loss(y_val, y_hat)
    
    return right, wrong, y_hat, log, roc, pr, brier

#Predict percentage of months in training data that had conflict.
def avg_model(X_train, y_train, X_val, y_val):
    y_val = y_val[:,-1]
    y_hat = []
    for c in np.unique(X_train[:,:,0]):
        seq = np.unique(np.argwhere(X_train[:,:,0] == c)[:,0])
        avg = np.mean(y_train[seq])


        #avg = np.zeros(y_val.shape[0]) + avg
        y_true = y_val
        val_seq = np.unique(np.argwhere(X_val[:,:,0] == c)[:,0])
        for s in val_seq:
            y_hat.append([y_true[s].reshape(-1,1),avg.reshape(-1,1)])

    y_hat = np.array(y_hat)
    log = metrics.log_loss(y_hat[:,0,0,0], y_hat[:,1,0,0])
    roc = metrics.roc_auc_score(y_hat[:,0,0,0], y_hat[:,1,0,0])
    pr = metrics.average_precision_score(y_hat[:,0,0,0],y_hat[:,1,0,0])
    brier = metrics.brier_score_loss(y_hat[:,0,0,0], y_hat[:,1,0,0])
    return log, roc, pr, brier, y_hat[:,1,0,0]

#Predict percentage of months in sample/sequence that had conflict.
def avg_sample_model(X_val, y_val, conflict, LAG, scaler):
    y_val = y_val[:,-1]
    y_hat = []
    for sample in range(0,X_val.shape[0]):
        track = 0
        for l in range(0,LAG):
            value = scaler.inverse_transform(X_val[sample,l][1:])[conflict-1]
            if value>0.5:
                track += 1

        avg = track/LAG
        y_hat.append(avg)

    y_hat = np.array(y_hat)
    log = metrics.log_loss(y_val, y_hat)
    roc = metrics.roc_auc_score(y_val, y_hat)
    pr = metrics.average_precision_score(y_val,y_hat)
    brier = metrics.brier_score_loss(y_val, y_hat)
    return log, roc, pr, brier, y_hat
    
#Turn sequence into a feature based sample.
def convert_data_to_rf(X):
    X_t = []
    for idx, x in enumerate(X):
        X_t.append(np.concatenate(x))

    return X_t 

#Random forest model.
def run_rf(X_train, y_train, X_val, y_val):
    y_val = y_val[:,-1]
    y_train = y_train[:,-1]
    X_tr = convert_data_to_rf(X_train)
    X_va = convert_data_to_rf(X_val)

    RandomForest = RandomForestClassifier(n_estimators=100, max_depth=4, max_features=15)
    RandomForest.fit(X_tr,y_train)

    y_hat = RandomForest.predict_proba(X_va)

    log = metrics.log_loss(y_val, y_hat[:,1])
    roc = metrics.roc_auc_score(y_val, y_hat[:,1])
    pr = metrics.average_precision_score(y_val, y_hat[:,1])
    brier = metrics.brier_score_loss(y_val, y_hat[:,1])
    return RandomForest, log, roc, pr, brier, y_hat[:,1]

#Gradient boost model.
def run_grboost(X_train, y_train, X_val, y_val):
    y_val = y_val[:,-1]
    y_train = y_train[:,-1]
    X_tr = convert_data_to_rf(X_train)
    X_va = convert_data_to_rf(X_val)

    Gradient = GradientBoostingClassifier(n_estimators=100, max_depth=4, max_features=15)
    Gradient.fit(X_tr,y_train)

    y_hat = Gradient.predict_proba(X_va)

    log = metrics.log_loss(y_val, y_hat[:,1])
    roc = metrics.roc_auc_score(y_val, y_hat[:,1])
    pr = metrics.average_precision_score(y_val, y_hat[:,1])
    brier = metrics.brier_score_loss(y_val, y_hat[:,1])
    return Gradient, log, roc, pr, brier

#XGBoost model.
def run_xgboost(X_train, y_train, X_val, y_val):
    y_val = y_val[:,-1]
    y_train = y_train[:,-1]
    X_tr = np.array(convert_data_to_rf(X_train))
    X_va = np.array(convert_data_to_rf(X_val))

    XGB = XGBClassifier(n_estimators=100, max_depth=4, max_features=15)
    XGB.fit(X_tr,y_train)

    y_hat = XGB.predict_proba(X_va)

    xgb.plot_importance(XGB)
    	
    print(XGB.feature_importances_)
    log = metrics.log_loss(y_val, y_hat[:,1])
    roc = metrics.roc_auc_score(y_val, y_hat[:,1])
    pr = metrics.average_precision_score(y_val, y_hat[:,1])
    brier = metrics.brier_score_loss(y_val, y_hat[:,1])
    return XGB, log, roc, pr, brier

#Logistic regression.
def run_logit(X_train, y_train, X_val, y_val):
    y_val = y_val[:,-1]
    y_train = y_train[:,-1]
    X_tr = convert_data_to_rf(X_train)
    X_va = convert_data_to_rf(X_val)

    logit = LogisticRegression()
    logit.fit(X_tr,y_train)

    y_hat = logit.predict_proba(X_va)

    log = metrics.log_loss(y_val, y_hat[:,1])
    roc = metrics.roc_auc_score(y_val, y_hat[:,1])
    pr = metrics.average_precision_score(y_val, y_hat[:,1])
    brier = metrics.brier_score_loss(y_val, y_hat[:,1])
    return logit, log, roc, pr, brier, y_hat[:,1]
