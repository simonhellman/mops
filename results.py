import matplotlib.pyplot as plt
from serealize import load_model_from_file, save_model
from data_preprocessing import preprocess
import seaborn as sns
from utils import load_samples, check_model_path
import numpy as np
from sklearn import metrics
from model import run_last_month, avg_model, avg_sample_model, run_rf, run_logit
import json


## loss, AUROC, AUPR and Brier score for a set of models (different steps).
def metric_over_months(lag, conflict, africa=False, batch_size=128,columns=[1,2,3,4]):
    scores = { }
    scores["lstm"] = {}
    scores["last_month"] = {}
    scores["average"] = {}
    scores["sample-avg"] = {}
    scores["logit"] = {}
    scores["randomforest"] = {}
    
    steps = [1,6,12]
    for s in steps:
        print("Step %d." % s)
        X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=lag, STEP=s, CONFLICT=conflict)
        
        X_train_col = X_train[:,:,columns]
        X_val_col = X_val[:,:,columns]
        X_train_all_col = X_train_all[:,:,columns]
        X_test_col = X_test[:,:,columns]
        
        ##When testing
        X_val_col = X_test_col
        X_val = X_test
        y_val = y_test
        
        
        
        print("Data done.")
        print("Load model.")
        check = check_model_path(lag,s,conflict,africa)
        if check == True:
            model = load_model_from_file(s,lag, conflict, africa)
        else:
            save_model(lag,s,conflict,africa,columns=columns)
            model = load_model_from_file(s,lag, conflict, africa)
        print("Model done.\n")
        
        remainder_val = X_val.shape[0]%batch_size
        #log, roc, pr = model.evaluate(X_test[remainder_val:],y_test[remainder_val:], verbose=0)
        #log = np.float64(log)
        #roc = np.float64(roc)
        #pr = np.float64(pr) 
        
        y_pred = model.predict(X_val_col[remainder_val:])
        y_pred = np.float64(y_pred)
        log = metrics.log_loss(y_val[remainder_val:,-1], y_pred[:,-1])
        roc = metrics.roc_auc_score(y_val[remainder_val:,-1], y_pred[:,-1])
        pr = metrics.average_precision_score(y_val[remainder_val:,-1],y_pred[:,-1])
        brier = metrics.brier_score_loss(y_val[remainder_val:,-1], y_pred[:,-1])
        if africa == True:
            model_critisism_plot_simple(y_val,y_pred, path=f"results/images/lstm_step_{s}_conflict_{conflict}_africa.png", lstm=True, batch_size=batch_size)
        else:
            model_critisism_plot_simple(y_val,y_pred, path=f"results/images/lstm_step_{s}_conflict_{conflict}.png", lstm=True, batch_size=batch_size)
        scores["lstm"][f"step {s}"] = {}
        scores["lstm"][f"step {s}"]["step"] = s
        scores["lstm"][f"step {s}"]["log"] = log
        scores["lstm"][f"step {s}"]["roc"] = roc
        scores["lstm"][f"step {s}"]["pr"] = pr
        scores["lstm"][f"step {s}"]["brier"] = brier
        
        
        print("Running 'last month'-model.")
        right, wrong, y_hat, log, roc, pr, brier = run_last_month(X=X_val, y_val=y_val, conflict=1, scaler=scaler)
        print("Model done.\n")
        if africa == True:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/last_month_step_{s}_conflict_{conflict}_africa.png", lstm=False)
        else:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/last_month_step_{s}_conflict_{conflict}.png", lstm=False)
        scores["last_month"][f"step {s}"] = {}
        scores["last_month"][f"step {s}"]["step"] = s
        scores["last_month"][f"step {s}"]["log"] = log
        scores["last_month"][f"step {s}"]["roc"] = roc
        scores["last_month"][f"step {s}"]["pr"] = pr
        scores["last_month"][f"step {s}"]["brier"] = brier
        
        print("Running average-model.")
        log, roc, pr, brier, y_hat = avg_model(X_train_all, y_train_all, X_val, y_val)
        print("Model done.\n")
        if africa == True:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/average_step_{s}_conflict_{conflict}_africa.png", lstm=False)
        else:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/average_step_{s}_conflict_{conflict}.png", lstm=False)
        scores["average"][f"step {s}"] = {}
        scores["average"][f"step {s}"]["step"] = s
        scores["average"][f"step {s}"]["log"] = log
        scores["average"][f"step {s}"]["roc"] = roc
        scores["average"][f"step {s}"]["pr"] = pr
        scores["average"][f"step {s}"]["brier"] = brier

        print("Running sample-avg-model.")
        log, roc, pr, brier, y_hat = avg_sample_model(X_val,y_val,conflict, lag, scaler)
        print("Model done.\n")
        if africa == True:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/sample-avg_step_{s}_conflict_{conflict}_africa.png", lstm=False)
        else:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/sample-avg_step_{s}_conflict_{conflict}.png", lstm=False)
        scores["sample-avg"][f"step {s}"] = {}
        scores["sample-avg"][f"step {s}"]["step"] = s
        scores["sample-avg"][f"step {s}"]["log"] = log
        scores["sample-avg"][f"step {s}"]["roc"] = roc
        scores["sample-avg"][f"step {s}"]["pr"] = pr
        scores["sample-avg"][f"step {s}"]["brier"] = brier

        print("Running logit.")
        model, log, roc, pr, brier, y_hat = run_logit(X_train_col, y_train, X_val_col, y_val)
        print("Model done.\n")
        if africa == True:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/logit_step_{s}_conflict_{conflict}_africa.png", lstm=False)
        else:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/logit_step_{s}_conflict_{conflict}.png", lstm=False)
        scores["logit"][f"step {s}"] = {}
        scores["logit"][f"step {s}"]["step"] = s
        scores["logit"][f"step {s}"]["log"] = log
        scores["logit"][f"step {s}"]["roc"] = roc
        scores["logit"][f"step {s}"]["pr"] = pr
        scores["logit"][f"step {s}"]["brier"] = brier

        print("Running Random Forest.")
        model, log, roc, pr, brier, y_hat = run_rf(X_train_col, y_train, X_val_col, y_val)
        print("Model done.\n")
        if africa == True:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/rf_step_{s}_conflict_{conflict}_africa.png", lstm=False)
        else:
            model_critisism_plot_simple(y_val,y_hat, path=f"results/images/rf_step_{s}_conflict_{conflict}.png", lstm=False)
        scores["randomforest"][f"step {s}"] = {}
        scores["randomforest"][f"step {s}"]["step"] = s
        scores["randomforest"][f"step {s}"]["log"] = log
        scores["randomforest"][f"step {s}"]["roc"] = roc
        scores["randomforest"][f"step {s}"]["pr"] = pr
        scores["randomforest"][f"step {s}"]["brier"] = brier 
    
    if africa == True:
        with open(f'results/result_metrics_conflict_{conflict}_africa.json', 'w') as fp:
            json.dump(scores, fp)
    else:
        with open(f'results/result_metrics_conflict_{conflict}.json', 'w') as fp:
            json.dump(scores, fp)

#Plot that shows distribution of forecasts for the two differnt classes.
def model_critisism_plot_simple(y_true, y_pred, path, lstm=False, batch_size=64):
    y_true = y_true[:,-1]
    if lstm == True:
        y_pred = y_pred[:,-1]
        remainder_val = y_true.shape[0]%batch_size
        args_1 = np.argwhere(y_true[remainder_val:] == 1)
        args_0 = np.argwhere(y_true[remainder_val:] == 0)
    else:
        args_1 = np.argwhere(y_true == 1)
        args_0 = np.argwhere(y_true == 0)
    sns.set()
    sns.set_color_codes()
    sns.distplot(y_pred[args_1])
    sns.distplot(y_pred[args_0])
    plt.savefig(path)
    plt.clf()

def lag_plot():
    lags = [6,12,24]
    step = 6
    conflict = 1
    africa = False
    batch_size = 64
    iterations = 20
    columns = [1,2,3,4,5,6,7,8,9,12,18,19,22,23,24,25,26,27,28]
    
    logs = np.zeros(len(lags))
    rocs = np.zeros(len(lags))
    prs = np.zeros(len(lags))
    briers = np.zeros(len(lags))
    for r in range(iterations):
        for idx,l in enumerate(lags):
            print("Lag %d." % l)
            X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=l, STEP=step, CONFLICT=conflict, columns=columns)
            
            X_train = X_train[:,:,columns]
            X_val = X_val[:,:,columns]
            X_train_all = X_train_all[:,:,columns]
            X_test = X_test[:,:,columns]

            save_model(l,step,conflict,africa,columns=columns)
            model = load_model_from_file(step,l, conflict, africa)

            remainder_val = X_val.shape[0]%batch_size

            y_pred = model.predict(X_val[remainder_val:])
            y_pred = np.float64(y_pred)

            log = metrics.log_loss(y_val[remainder_val:,-1], y_pred[:,-1])
            roc = metrics.roc_auc_score(y_val[remainder_val:,-1], y_pred[:,-1])
            pr = metrics.average_precision_score(y_val[remainder_val:,-1],y_pred[:,-1])
            brier = metrics.brier_score_loss(y_val[remainder_val:,-1], y_pred[:,-1])
            
            print(log)
            print(logs)
            logs[idx] += log
            print(logs)
            rocs[idx] += roc
            prs[idx] += pr
            briers[idx] += brier
            
    logs = logs/iterations
    rocs = rocs/iterations
    prs = prs/iterations
    briers = briers/iterations
    
    return logs,rocs,prs,briers
    
def batch_plot():
    lag = 12
    step = 6
    conflict = 1
    africa = False
    batch_sizes = [16,32,64,128,256,512]
    iterations = 10
    learning_rates = [1e-1,1e-2,1e-3,1e-4,1e-5,1e-6,1e-7]
    columns = [1,2,3,4,5,6,7,8,9,12,18,19,22,23,24,25,26,27,28]
    
    logs = np.zeros(len(batch_sizes))
    rocs = np.zeros(len(batch_sizes))
    prs = np.zeros(len(batch_sizes))
    briers = np.zeros(len(batch_sizes))
    for r in range(iterations):
        for idx,b in enumerate(batch_sizes):
            print("Batch size %d." % b)
            X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=lag, STEP=step, CONFLICT=conflict)

            X_train = X_train[:,:,columns]
            X_val = X_val[:,:,columns]
            X_train_all = X_train_all[:,:,columns]
            X_test = X_test[:,:,columns]

            save_model(lag,step,conflict,africa,b,columns=columns)
            model = load_model_from_file(step,lag, conflict, africa)

            remainder_val = X_val.shape[0]%b

            y_pred = model.predict(X_val[remainder_val:])
            y_pred = np.float64(y_pred)

            log = metrics.log_loss(y_val[remainder_val:,-1], y_pred[:,-1])
            roc = metrics.roc_auc_score(y_val[remainder_val:,-1], y_pred[:,-1])
            pr = metrics.average_precision_score(y_val[remainder_val:,-1],y_pred[:,-1])
            brier = metrics.brier_score_loss(y_val[remainder_val:,-1], y_pred[:,-1])
            
            print(log)
            print(logs)
            logs[idx] += log
            print(logs)
            rocs[idx] += roc
            prs[idx] += pr
            briers[idx] += brier
            
    logs = logs/iterations
    rocs = rocs/iterations
    prs = prs/iterations
    briers = briers/iterations
    
    return logs,rocs,prs,briers    

def learning_rate_plot():
    lag = 12
    step = 6
    conflict = 1
    africa = False
    batch_size = 128
    iterations = 20
    learning_rates = [1e-2,1e-3,1e-4]
    
    
    logs = np.zeros(len(learning_rates))
    rocs = np.zeros(len(learning_rates))
    prs = np.zeros(len(learning_rates))
    briers = np.zeros(len(learning_rates))
    columns = [1,2,3,4,9,12,18,19,20,21,24,25,26]
    for r in range(iterations):
        for idx,l in enumerate(learning_rates):
            print("Learning rate %f." % l)
            X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=lag, STEP=step, CONFLICT=conflict)
            X_train = X_train[:,:,columns]
            X_val = X_val[:,:,columns]
            X_train_all = X_train_all[:,:,columns]
            X_test = X_test[:,:,columns]

            save_model(lag,step,conflict,africa,batch_size,learning_rate=l,columns=columns)
            model = load_model_from_file(step,lag, conflict, africa)

            remainder_val = X_val.shape[0]%batch_size

            y_pred = model.predict(X_val[remainder_val:])
            y_pred = np.float64(y_pred)

            log = metrics.log_loss(y_val[remainder_val:,-1], y_pred[:,-1])
            roc = metrics.roc_auc_score(y_val[remainder_val:,-1], y_pred[:,-1])
            pr = metrics.average_precision_score(y_val[remainder_val:,-1],y_pred[:,-1])
            brier = metrics.brier_score_loss(y_val[remainder_val:,-1], y_pred[:,-1])
            
            print(log)
            print(logs)
            logs[idx] += log
            print(logs)
            rocs[idx] += roc
            prs[idx] += pr
            briers[idx] += brier
            
    logs = logs/iterations
    rocs = rocs/iterations
    prs = prs/iterations
    briers = briers/iterations
    
    return logs,rocs,prs,briers    



    
# You have to manually change the network in model.py. This is just for either vector or non-vector.
def y_vector_test():
    lag = 12
    step = 6
    conflict = 1
    africa = False
    batch_size = 128
    iterations = 40
    
    logs = 0
    rocs = 0
    prs = 0
    briers = 0
    
    columns = [1,2,3,4,5,6,7,8,9,12,18,19,22,23,24,25,26,27,28]
    for r in range(iterations):

        print("Iteration %d." % r)
        X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=lag, STEP=step, CONFLICT=conflict)
        
        X_train = X_train[:,:,columns]
        X_val = X_val[:,:,columns]
        X_train_all = X_train_all[:,:,columns]
        X_test = X_test[:,:,columns]
        #y_train = y_train[:,-1]
        #y_train_all = y_train_all[:,-1]
        #y_val = y_val[:,-1]
        #y_test = y_test[:,-1]   

        save_model(lag,step,conflict,africa,batch_size,columns)
        model = load_model_from_file(step,lag, conflict, africa)

        remainder_val = X_val.shape[0]%batch_size

        y_pred = model.predict(X_val[remainder_val:])
        y_pred = np.float64(y_pred)

        log = metrics.log_loss(y_val[remainder_val:,-1], y_pred[:,-1])
        roc = metrics.roc_auc_score(y_val[remainder_val:,-1], y_pred[:,-1])
        pr = metrics.average_precision_score(y_val[remainder_val:,-1],y_pred[:,-1])
        brier = metrics.brier_score_loss(y_val[remainder_val:,-1], y_pred[:,-1])
        
        print(log)
        print(logs)
        logs += log
        print(logs)
        rocs += roc
        prs += pr
        briers += brier
        
    logs = logs/iterations
    rocs = rocs/iterations
    prs = prs/iterations
    briers = briers/iterations
    
    return logs,rocs,prs,briers          
    
def main():
    lag = 12
    africa = True
    for conflict in [1,2,3]:
        metric_over_months(lag,conflict, africa, batch_size=128,columns=[1,2,3,4,9,12,18,19,20,21,24,25,26])
    
    
    
    
if __name__ == "__main__":
    main()
    
