import kerastuner as kt
from kerastuner.tuners import BayesianOptimization, RandomSearch
from kerastuner.engine.hyperparameters import HyperParameters
import time
from data_preprocessing import preprocess
from model import tuner_model
from tensorflow.keras.callbacks import EarlyStopping

### PARAMETERS #####
LAG = 12
STEP = 6
CONFLICT = 1

EPOCHS = 50
BATCH_SIZE = 128

print("Process data.\n")
X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=LAG, STEP=STEP, CONFLICT=CONFLICT, check_for_data=True)
print("Data done.\n")


y_train = y_train[:,-1]
y_val = y_val[:,-1]
y_test = y_test[:,-1]

remainder_train = X_train.shape[0]%BATCH_SIZE
remainder_val = X_val.shape[0]%BATCH_SIZE

X_train = X_train[:,:,[1,2,3,4,5,6,7,8,9,12,18,19,22,23,24,25,26,27,28]]
X_val = X_val[:,:,[1,2,3,4,5,6,7,8,9,12,18,19,22,23,24,25,26,27,28]]
X_train = X_train[remainder_train:]
y_train = y_train[remainder_train:]
X_val = X_val[remainder_val:]
y_val = y_val[remainder_val:]




es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=10)

LOG_DIR = f"{int(time.time())}"
tuner = BayesianOptimization(
    tuner_model,
    objective = "val_loss",
    max_trials = 50,
    executions_per_trial = 3,
    directory = LOG_DIR
    )

tuner.search(x=X_train,
            y=y_train,
            epochs=100,
            batch_size=BATCH_SIZE,
            validation_data=(X_val, y_val),
            callbacks=[es],
            verbose=2)

tuner.results_summary()