import pandas as pd
import numpy as np
from datetime import datetime
from dateutil import relativedelta
from countryinfo import CountryInfo
import pycountry
from tqdm import tqdm
import json
from utils import date_to_month, sort_by_date, convert_to_months, data_clean_dates


#Load and combine data from Syria and the rest of the world.
def load_data():
    
    files = ["data/UCDP/ged191.csv", "data/UCDP/ged_syria.csv"]
    
    columns = ["date_start", "date_end", "country", "type_of_violence", "conflict_new_id", "dyad_new_id", "side_a_new_id", "gwnoa", "gwnob",  
           "side_b_new_id", "country_id", "adm_1", "deaths_a", "deaths_b", "deaths_civilians", 
           "deaths_unknown"]
    
    file = pd.read_csv(files[0])
    file_syria = pd.read_csv(files[1])
    file = file.append(file_syria, sort=False, ignore_index=True)
    return file[columns]

### gjort 25/3
def many_month_events(data):
    events = data[data["date_start"] < data["date_end"]].copy()
    
    tmp = pd.DataFrame(columns=events.columns)
    for i in events.iterrows():
        for iter in range(0,11):
            if i[1][0] < i[1][1]:
                i[1][0] += 1
                tmp = tmp.append(i[1])
            
    events = events.append(tmp)
    
    indices = pd.unique(events.index)

    for i in indices:
        size = events[events.index==i].shape[0]
        events.loc[events.index==i, "deaths_a"] = events.loc[events.index==i, "deaths_a"]/size
        events.loc[events.index==i, "deaths_b"] = events.loc[events.index==i, "deaths_b"]/size
        events.loc[events.index==i, "deaths_civilians"] = events.loc[events.index==i, "deaths_civilians"]/size
        events.loc[events.index==i, "deaths_unknown"] = events.loc[events.index==i, "deaths_unknown"]/size
        
    data = data.drop(indices)
    data = data.append(events, ignore_index=True)
    
    return data

def data_events(data):
    country_count = pd.DataFrame(data.groupby(['date_start', 'country_id', 'type_of_violence']).size())
    COUNTRIES_csv = pd.read_csv("data/country/country_ids.csv")
    COUNTRIES_read = COUNTRIES_csv.to_numpy()
    COUNTRIES = COUNTRIES_read
    num_countries = len(COUNTRIES_csv)
    num_months = 360
    COUNTRY_LIST = np.zeros((num_countries*360,6))

    #Country and number of deaths each month 
    country_death = data[["date_start", "country_id", "deaths_a", "deaths_b", "deaths_civilians", "deaths_unknown"]].groupby(["date_start", "country_id"]).sum()
    country_death = country_death.sum(axis=1)


    while len(COUNTRIES) < num_countries*360:
        COUNTRIES = np.append(COUNTRIES, COUNTRIES_read, axis=0)

    month = np.zeros(num_countries)
    i=1
    while len(month) < num_countries*360:
        month = np.append(month, np.zeros(num_countries)+i, axis=0)
        i=i+1

    COUNTRY_LIST[:,1] = COUNTRIES[:,1]
    COUNTRY_LIST[:,0] = month

    ## Loop to insert all events and deaths into the right country and month.
    at_index = 2
    for i in range(0,num_months):
        for j in country_count[0][i].index:
            try:
                idx = num_countries*i+COUNTRIES_csv[COUNTRIES_csv["country_id"] == j[0]].index.tolist()[0]
            except:
                print(j[0])
            if j[1] == 1:
                COUNTRY_LIST[idx,at_index] = country_count[0][i][j]
            if j[1] == 2:
                COUNTRY_LIST[idx,at_index+1] = country_count[0][i][j]
            if j[1] == 3:
                COUNTRY_LIST[idx,at_index+2] = country_count[0][i][j]
            COUNTRY_LIST[idx,at_index+3] = country_death[i][j[0]]

    return COUNTRY_LIST

def data_event_dummy(COUNTRY_LIST,index):
    COUNTRY_LIST_new = np.append(COUNTRY_LIST, np.zeros([len(COUNTRY_LIST),3]),axis=1)
    
    for i in range(0,len(COUNTRY_LIST_new)):
        
        
        at_idx = index
        if COUNTRY_LIST_new[i,at_idx] != 0:
            COUNTRY_LIST_new[i,at_idx+4] = 1
            
        if COUNTRY_LIST_new[i,at_idx+1] != 0:
            COUNTRY_LIST_new[i,at_idx+5] = 1
            
        if COUNTRY_LIST_new[i,at_idx+2] != 0:
            COUNTRY_LIST_new[i,at_idx+6] = 1

    return COUNTRY_LIST_new


#Full country name from country code
def code_to_countries(code):
    countries = []
    for idx, i in enumerate(code):
        try:
            countries.append(pycountry.countries.get(alpha_3=i).name)
        except:
            a=1
    return countries

#Check if name is faulty
def check_names(name):
    if name == "DR Congo (Zaire)":
        name = "Democratic Republic of the Congo"
        return name
    elif name == "Russia (Soviet Union)":
        name = "Russia"
        return name
    elif name == "Yemen (North Yemen)":
        name = "Yemen"
        return name
    elif name == "Cambodia (Kampuchea)":
        name = "Cambodia"
        return name
    elif name == "Congo":
        name = "Republic of the Congo"
        return name
    elif name == "Zimbabwe (Rhodesia)":
        name = "Zimbabwe"
        return name
    elif name == "Bosnia-Herzegovina":
        name = "Bosnia and Herzegovina"
        return name
    elif name == "Macedonia, FYR":
        name = "Republic of Macedonia"
        return name
    elif name == "Kingdom of eSwatini (Swaziland)":
        name = "Swaziland"
        return name
    elif name == "Rumania":
        name = "Romania"
        return name
    elif name == "United States of America":
        name = "United States"
        return name
    elif name == "Gambia":
        name = "The Gambia"
        return name
    elif name == "Madagascar (Malagasy)":
        name = "Madagascar"
        return name
    elif name == "Bahamas":
        name = "The Bahamas"
        return name
    elif name == "St. Lucia":
        name = "Saint Lucia"
        return name
    elif name == "St. Vincent and the Grenadines":
        name = "Saint Vincent and the Grenadines"
        return name
    elif name == "Antigua & Barbuda":
        name = "Antigua and Barbuda"
        return name
    elif name == "St. Kitts and Nevis":
        name = "Saint Kitts and Nevis"
        return name
    elif name == "Sao Tome and Principe":
        name = "São Tomé and Príncipe"
        return name
    else:
        return name
    
#Fix faulty names.
def fix_names(list):
    names = list["country"]
    for idx,c in enumerate(list["country"]):
        names[idx] = check_names(c)
    list["country"] = names
    return list

def data_nb_events(COUNTRY_LIST):
    COUNTRY_LIST_new = np.append(COUNTRY_LIST, np.zeros([len(COUNTRY_LIST),4]),axis=1)
    COUNTRIES_csv = fix_names(pd.read_csv("data/country/country_ids.csv"))
    num_countries = len(COUNTRIES_csv)
    with open("data/country/data.json", "r") as read_file:
        country = json.load(read_file)

    for i in tqdm(range(0,len(COUNTRY_LIST_new))):
        name = COUNTRIES_csv[COUNTRIES_csv["country_id"] == COUNTRY_LIST_new[i,1]]["country"][i%num_countries]

        if name == "Myanmar (Burma)":
            countries = ['Bangladesh', 'India', 'Laos', 'Thailand', 'China']
        elif name == "Serbia (Yugoslavia)":
            countries = ["Croatia", "Bosnia and Herzegovina", "Kosovo", "Republic of Macedonia", "Bulgaria", "Romania", "Hungary"]
        elif name == "Montenegro":
            countries = ["Croatia", "Bosnia and Herzegovina", "Serbia", "Albania"]
        elif name == "Macedonia":
            countries = ["Serbia", "Bulgaria", "Greece", "Albania"]
        elif name == "Andorra":
            countries = ["France", "Spain"]
        else:
            countries = code_to_countries(country[name.lower()]["borders"]) 
        at_idx = 9
        for c in countries:
            month = int(COUNTRY_LIST_new[i,0])
            try:
                row = COUNTRIES_csv[COUNTRIES_csv["country"] == c]["country_id"].index[0]
                COUNTRY_LIST_new[i,at_idx] += COUNTRY_LIST_new[(month*num_countries)+row,at_idx-7]
                COUNTRY_LIST_new[i,at_idx+1] += COUNTRY_LIST_new[(month*num_countries)+row,at_idx-6]
                COUNTRY_LIST_new[i,at_idx+2] += COUNTRY_LIST_new[(month*num_countries)+row,at_idx-5]
                COUNTRY_LIST_new[i,at_idx+3] += COUNTRY_LIST_new[(month*num_countries)+row,at_idx-4]
            except:
                a=1
                
    return COUNTRY_LIST_new


def conflict_clean():
    stage1 = load_data()
    print("#### Stage 1 - loading data - done ####")
    stage2 = data_clean_dates(stage1, columns=["date_start", "date_end"], format='%Y-%m-%d')
    print("#### Stage 2 - clean dates - done ####")
    stageX = many_month_events(stage2)
    print("#### Stage X - clean dates - done ####")
    stage3 = data_events(stageX)
    print("#### Stage 3 - fill events - done ####")
    stage4 = data_event_dummy(stage3, 2)
    print("#### Stage 4 - event dummy - done ####")
    stage5 = data_nb_events(stage4)
    print("#### Stage 5 - neighbour event - done ####")
    stage6 = data_event_dummy(stage5, 9)
    print("#### Stage 6 - neighbour event dummy - done ####")
    print("#### FINISHED ####")
    return stage6



#######################################
# Method for country_ids.csv

def countries():
    countries = pd.read_csv("data/country/COW country codes.csv")
    countries = countries[["StateNme", "CCode"]].drop([55,56,59,60,61,62,63,64,65,66,69,73,74,78,79,80,81,82,83,93,147,189,190,205,222]).rename(columns={"StateNme": "country", "CCode": "country_id"})
    countries.loc[countries["country_id"] == 255, "country_id"] = 260
    countries.loc[countries["country"] == "Yugoslavia", "country"] = "Serbia (Yugoslavia)"
    countries.loc[countries["country"] == "Yemen Arab Republic", "country"] = "Yemen"
    countries.loc[countries["country"] == "Myanmar", "country"] = "Myanmar (Burma)"
    countries.drop_duplicates(inplace=True)
    # Drop certain countries for simplicity
    #
    # Countries (from V-Dem):
    # [ 31.,  54.,  55.,  56.,  57.,  58.,  60.,  80., 221., 223., 232.,
    #   835., 946., 947., 955., 970., 983., 986., 987., 990.]
    #
    # [316., 317., 341., 343., 344., 346., 349., 359., 366., 367., 368.,
    #   369., 370., 371., 372., 373., 531., 626., 701., 702., 703., 704.,
    #   705., 860.]
    #
    #
    countries.drop(countries[countries["country_id"] == 31].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 54].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 55].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 56].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 57].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 58].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 60].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 80].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 221].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 223].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 232].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 316].index, inplace=True)
    #countries.drop(countries[countries["country_id"] == 317].index, inplace=True) # Slovaika
    #countries.drop(countries[countries["country_id"] == 341].index, inplace=True) # Montenegro
    #countries.drop(countries[countries["country_id"] == 343].index, inplace=True) # Macedonia
    #countries.drop(countries[countries["country_id"] == 344].index, inplace=True) # Croatia
    #countries.drop(countries[countries["country_id"] == 346].index, inplace=True) # Bosnia and Herzegovina
    #countries.drop(countries[countries["country_id"] == 349].index, inplace=True) # Slovenia
    #countries.drop(countries[countries["country_id"] == 359].index, inplace=True) # Moldova
    #countries.drop(countries[countries["country_id"] == 366].index, inplace=True) # Estonia
    #countries.drop(countries[countries["country_id"] == 367].index, inplace=True) # Latvia
    #countries.drop(countries[countries["country_id"] == 368].index, inplace=True) # Lithuania
    #countries.drop(countries[countries["country_id"] == 369].index, inplace=True) # Ukraine
    #countries.drop(countries[countries["country_id"] == 370].index, inplace=True) # Belarus
    #countries.drop(countries[countries["country_id"] == 371].index, inplace=True) # Armenia
    #countries.drop(countries[countries["country_id"] == 372].index, inplace=True) # Georgia
    #countries.drop(countries[countries["country_id"] == 373].index, inplace=True) # Azerbaijan
    countries.drop(countries[countries["country_id"] == 403].index, inplace=True)
    #countries.drop(countries[countries["country_id"] == 531].index, inplace=True) # Eritrea
    #countries.drop(countries[countries["country_id"] == 626].index, inplace=True) # South sudan
    #countries.drop(countries[countries["country_id"] == 701].index, inplace=True) # Turkmenistan
    #countries.drop(countries[countries["country_id"] == 702].index, inplace=True) # Taijkistan
    #countries.drop(countries[countries["country_id"] == 703].index, inplace=True) # Kyrgystan
    #countries.drop(countries[countries["country_id"] == 704].index, inplace=True) # Uzbekistan
    #countries.drop(countries[countries["country_id"] == 705].index, inplace=True) # Kazakhstan
    countries.drop(countries[countries["country_id"] == 713].index, inplace=True) # Taiwan (no conflict events)
    countries.drop(countries[countries["country_id"] == 835].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 860].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 935].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 940].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 946].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 947].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 950].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 955].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 970].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 983].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 986].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 987].index, inplace=True)
    countries.drop(countries[countries["country_id"] == 990].index, inplace=True)
    
    countries.to_csv("data/country/country_ids.csv", index=False)
    
    
#######################################################################
########################### V-DEM  ####################################
    
def load_vdem():
    vdem = pd.read_csv("data/V-Dem/V-Dem-CY-Full+Others-v9.csv")
    columns = ["country_name", "year", "COWcode", "v2x_regime", "e_wb_pop", "e_miferrat", "e_peaveduc", "e_area", "e_pelifeex", "e_peinfmor", "e_pt_coup", "v2x_clphy", "v2x_rule", "e_regiongeo", "e_migdpgro", "e_migdppc",
                "e_cow_exports", "e_cow_imports"]
    
    ## Fix Germany code in V-Dem
    vdem.loc[vdem["COWcode"] == 255, "COWcode"] = 260
    
    #"v2xpe_exlecon", "v2xpe_exlgender", "v2xpe_exlgeo", "v2xpe_exlsocgr", "v2x_corr", "v2x_gender", "e_total_resources_income_pc"
    #vdem_ny.to_csv("data/V-Dem/vdem_columns_new_region.csv")
    return vdem[columns]

def load_countries():
    countries = pd.read_csv("data/country/country_ids.csv")
    return countries

def montly_vdem(countries):
    COUNTRIES = countries.to_numpy()
    COUNTRIES_static = COUNTRIES
    num_con = len(countries)
    num_col = 17
    num_months = 360
    LIST = np.zeros([num_con*num_months,num_col])

    while len(COUNTRIES) < num_con*num_months:
        COUNTRIES = np.append(COUNTRIES, COUNTRIES_static, axis=0)

    month = np.zeros(num_con)
    i=1
    while len(month) < num_con*num_months:
        month = np.append(month, np.zeros(num_con)+i, axis=0)
        i=i+1

    LIST[:,2] = -99 # Placeholder for NaN
    LIST[:,1] = COUNTRIES[:,1]
    LIST[:,0] = month

    return LIST
    
     ## Loop to insert all columns into the right country and month.
def insert_vdem(LIST, vdem, countries, columns, index, year):
    at_index = index
    num_con = len(countries)
    num_months = 360
    for i in tqdm(range(0,num_months,12)):
        for j in countries.iterrows():
            idx = num_con*i+countries[countries["country_id"] == j[1][1]].index.tolist()[0]
            
            try:
                LIST[idx,at_index:(at_index+len(columns))] = vdem[vdem["COWcode"] == LIST[idx,:][1]][vdem["year"] == int((LIST[idx,:][0]/12)+year)][columns].to_numpy()
            except:
                pass
                    
    return LIST
                    
def insert_vdem_final(LIST, countries, vdem):
    columns1 = ["v2x_regime", "e_wb_pop", "e_miferrat", "e_peaveduc", "e_area", "e_pelifeex", "e_peinfmor", "v2x_clphy", "v2x_rule", "e_regiongeo"] # remember region
    columns2 = ["e_migdpgro", "e_migdppc", "e_cow_exports", "e_cow_imports"]
    columns3 = ["e_pt_coup"]
            
    LIST = insert_vdem(LIST, vdem, countries, columns1, 2, 1989)
    LIST = insert_vdem(LIST, vdem, countries, columns2, 12, 1989)
    LIST = insert_vdem(LIST, vdem, countries, columns3, 16, 1988)
    
    return LIST
    
    
def interpolate_vdem(LIST, countries):
    s = pd.DataFrame(LIST, columns=["month", "country_id", "v2x_regime", "e_wb_pop", "e_miferrat", "e_peaveduc", "e_area", "e_pelifeex", "e_peinfmor",
                            "v2x_clphy", "v2x_rule", "e_regiongeo", "e_migdpgro", "e_migdppc", "e_cow_exports", "e_cow_imports", "e_pt_coup"])
        
    for c in countries.iterrows():
        s.loc[s["country_id"]==c[1][1], "v2x_regime"] = s.loc[s["country_id"]==c[1][1], "v2x_regime"].replace(-99, np.NaN)
        s.loc[s["country_id"]==c[1][1], "v2x_regime"] = s.loc[s["country_id"]==c[1][1], "v2x_regime"].fillna(method='ffill')
        s.loc[s["country_id"]==c[1][1], "v2x_regime"] = s.loc[s["country_id"]==c[1][1], "v2x_regime"].fillna(method='bfill')
        s.loc[s["country_id"]==c[1][1], "v2x_regime"] = s.loc[s["country_id"]==c[1][1], "v2x_regime"].round(0)
        s.loc[s["country_id"]==c[1][1], "e_pt_coup"] = s.loc[s["country_id"]==c[1][1], "e_pt_coup"].fillna(method='ffill')
        s.loc[s["country_id"]==c[1][1], "e_pt_coup"] = s.loc[s["country_id"]==c[1][1], "e_pt_coup"].fillna(method='bfill')
        s.loc[s["country_id"]==c[1][1], "e_regiongeo"] = s.loc[s["country_id"]==c[1][1], "e_regiongeo"].fillna(method='ffill')
        s.loc[s["country_id"]==c[1][1], "e_regiongeo"] = s.loc[s["country_id"]==c[1][1], "e_regiongeo"].fillna(method='bfill')
        s.loc[s["country_id"]==c[1][1], "e_regiongeo"] = s.loc[s["country_id"]==c[1][1], "e_regiongeo"].round(0)
        
        columns = ["e_wb_pop", "e_miferrat", "e_peaveduc", "e_area", "e_pelifeex", "e_peinfmor", "v2x_clphy", "v2x_rule", "e_regiongeo", "e_migdpgro", "e_migdppc", "e_cow_exports"
                   , "e_cow_imports"]
        for i in columns:
            s.loc[s["country_id"]==c[1][1], i] = s.loc[s["country_id"]==c[1][1], i].replace(0, np.NaN)
            try:
                s.loc[s["country_id"]==c[1][1], i] = s.loc[s["country_id"]==c[1][1], i].interpolate(method="quadratic", limit_direction="both", limit_area="inside")
            except:
                pass
            try:
                s.loc[s["country_id"]==c[1][1], i] = s.loc[s["country_id"]==c[1][1], i].interpolate(method="spline", limit_direction="both", order=1)
            except:
                pass
            s.loc[s["country_id"]==c[1][1], i] = s.loc[s["country_id"]==c[1][1], i].interpolate(method="linear", limit_direction="both", axis=0)

        no_region = [212, 338, 692, 696]
        s.loc[s["country_id"]==212, "e_regiongeo"] = s.loc[s["country_id"]==212, "e_regiongeo"].replace(np.NaN, 1)
        s.loc[s["country_id"]==338, "e_regiongeo"] = s.loc[s["country_id"]==338, "e_regiongeo"].replace(np.NaN, 3)
        s.loc[s["country_id"]==692, "e_regiongeo"] = s.loc[s["country_id"]==692, "e_regiongeo"].replace(np.NaN, 10)
        s.loc[s["country_id"]==696, "e_regiongeo"] = s.loc[s["country_id"]==696, "e_regiongeo"].replace(np.NaN, 10)

        # Regions taken from https://data.un.org/CountryProfile.aspx/_Images/CountryProfile.aspx?crName=United%20Arab%20Emirates
        
    return s
    
def region_mean(data, vdem):

    v = vdem.groupby(["year", "e_regiongeo"]).mean()
    columns=["month", "country_id", "v2x_regime", "e_wb_pop", "e_miferrat", "e_peaveduc", "e_area", "e_pelifeex", "e_peinfmor",
                            "v2x_clphy", "v2x_rule", "e_regiongeo", "e_migdpgro", "e_migdppc", "e_cow_exports", "e_cow_imports", "e_pt_coup"]
    
    #v = vdem.groupby(["year", "e_regiongeo"])["e_miferrat"].mean()
    for index, row in data.iterrows():
        
        for i, c in enumerate(columns):
            if np.isnan(row[i]) and row[0]%12==0:
                try:
                    row[i] = v[c][int(1989+row[0]/12)][row[11]]
                except:
                    print("Error: no data for region.")                      
    return data
            
def vdem_clean():
    countries = load_countries()
    vdem = load_vdem()
    monthly = montly_vdem(countries)
    
    print("Starting stage 1")
    stage1 = insert_vdem_final(monthly, countries, vdem)
    print("Starting stage 2")
    stage2 = interpolate_vdem(stage1, countries)
    print("Starting stage 3")
    stage3 = region_mean(stage2, vdem)
    print("Starting stage 4")
    stage4 = interpolate_vdem(stage3, countries)
    print("### FINISHED ###")
        
    return stage4
        
        
        
################################################### ACLED #####################################

def load_acled():
    data = pd.read_csv("data/ACLED/acled.csv")
    data.loc[data["iso"] == 0, "iso"] = 688
    data.loc[data["iso"] == 275, "iso"] = 376
    return data

def data_acled_events(data):
    country_count = pd.DataFrame(data.groupby(['event_date', 'iso', "event_type"]).size())
    COUNTRIES_csv = pd.read_csv("data/country/country_ids_all.csv")
    COUNTRIES_read = COUNTRIES_csv.to_numpy()
    COUNTRIES = COUNTRIES_read
    num_countries = len(COUNTRIES_csv)
    num_months = 360
    COUNTRY_LIST = np.zeros((num_countries*360,4))

    while len(COUNTRIES) < num_countries*360:
        COUNTRIES = np.append(COUNTRIES, COUNTRIES_read, axis=0)

    month = np.zeros(num_countries)
    i=1
    while len(month) < num_countries*360:
        month = np.append(month, np.zeros(num_countries)+i, axis=0)
        i=i+1

    COUNTRY_LIST[:,1] = COUNTRIES[:,2]
    COUNTRY_LIST[:,0] = month

    ## Loop to insert all events and deaths into the right country and month.
    at_index = 2
    for i in tqdm(range(0,num_months)):
        try:
            for j in country_count[0][i].index:
                try:
                    idx = num_countries*i+COUNTRIES_csv[COUNTRIES_csv["iso"] == j[0]].index.tolist()[0]
                except:
                    print(j[0])
                
                if j[1] == "Protests":
                    COUNTRY_LIST[idx,at_index] = country_count[0][i][j]
                if j[1] == "Riots":
                    COUNTRY_LIST[idx,at_index+1] = country_count[0][i][j]
        except:
            pass
        
    for row in COUNTRY_LIST:
        row[1] = COUNTRIES_csv[COUNTRIES_csv["iso"] == row[1]]["country_id"]
    return COUNTRY_LIST


def acled_clean():
    
    stage1 = load_acled()
    print("#### Stage 1 - loading data - done ####")
    stage2 = data_clean_dates(stage1, ["event_date"], format='%d %B %Y')
    print("#### Stage 2 - clean dates - done ####")
    stage3 = data_acled_events(stage2)
    print("#### Stage 3 - insert events - done ####")
    print("#### FINISHED ####")

    return stage3