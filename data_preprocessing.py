import numpy as np
import pandas as pd
from sklearn import preprocessing
from collections import deque
import random
from utils import check_path, load_samples, load_scaler
import joblib

"""Split data into train, test and val."""
def split_data(data):
    train = data[0:int(len(data)*0.7)]
    val = data[int(len(data)*0.7):int(len(data)*0.82)]
    test = data[int(len(data)*0.82):]
    ## test data is approx ViEWS time.
    
    train.to_csv("data/processed/model/train.csv", index=False)
    val.to_csv("data/processed/model/val.csv", index=False)
    test.to_csv("data/processed/model/test.csv", index=False)
    return train, val, test


"""Merge conflict and vdem data."""
def concat_data():
    vdem = pd.read_csv("data/processed/vdem_only.csv")
    conflict = pd.read_csv("data/processed/conflict_only.csv")
    acled = pd.read_csv("data/processed/acled_only.csv")
        
    result = pd.concat([conflict, vdem, acled], axis=1, sort=False)
    
    column_selector = ~result.columns.duplicated()
    result = result.loc[:, column_selector]
    
    result.to_csv("data/processed/full_dataset.csv", index=False)
        
    return result

"""Create samples with normalized data."""
def create_sample(lag, step, conflict, africa):
    sequential_data = []

    #Read data
    train = pd.read_csv("data/processed/model/train.csv")
    val = pd.read_csv("data/processed/model/val.csv")
    test = pd.read_csv("data/processed/model/test.csv")
    test = test[test["month"]<336]
    
    scaler = preprocessing.StandardScaler()
    for count, data in enumerate([train, val, test]):
        if africa == True:
            if count == 2:
                data = data[data["e_regiongeo"].isin([5,6,7,8,9])]
        
        #Create dummies for columns
        data = pd.get_dummies(data, columns=["v2x_regime"], drop_first=True)
        data = pd.get_dummies(data, columns=["e_pt_coup"], drop_first=True) 
        #data = pd.get_dummies(data, columns=["e_regiongeo"], drop_first=True) #did not work well 
        
        #Which columns should be excluded. country_id is important for avg-models so it needs to remain. Later more features can be removed in the modelling.
        not_columns = ["month", "country_id", "dummy_type_1", "dummy_type_2", "dummy_type_3", "dummy_nb_type_1", "dummy_nb_type_2", "dummy_nb_type_3"]#,
                       #"e_peaveduc", "e_pelifeex", "e_peinfmor", "v2x_clphy", "v2x_rule", "e_miferrat", "e_migdpgro", "e_cow_exports", 
                       #"e_cow_imports"]
        column_selector = data.columns.difference(not_columns, sort=False)
        not_columns.remove("country_id")
        
        #Check if train data and scale.
        if count == 0:
            scaler.fit(data.loc[:,column_selector])
            data.loc[:,column_selector] = scaler.transform(data.loc[:,column_selector])
        else:
            data.loc[:,column_selector] = scaler.transform(data.loc[:,column_selector])
            
        #Extract every country and shift the dummies in other to create a "y".
        for c in data["country_id"].unique():
            tmp_data = data[data["country_id"]==c].copy()
        
            for s in range(1,step+1):
                tmp_data[f"TARGET__step_{s}"] = tmp_data[f"dummy_type_{conflict}"].shift(-s)
                #Remove these and only use one conflict type at the time.
                #tmp_data[f"TARGET_type_2_step_{s}"] = tmp_data["dummy_type_2"].shift(-s)
                #tmp_data[f"TARGET_type_3_step_{s}"] = tmp_data["dummy_type_3"].shift(-s)

            tmp_data = tmp_data.drop(columns=not_columns) 
            tmp_data.dropna(inplace=True)
            prev_months = deque(maxlen=lag)

            #Create sequences.
            for i in tmp_data.values:
                
                prev_months.append([n for n in i[:-step]])
                if len(prev_months) == lag:
                    sequential_data.append([np.array(prev_months), np.array(i[-step:])])

        random.shuffle(sequential_data)
        
        print("All data: %d" % len(sequential_data))

        #Check how many conflict event and non-conflict events there are. Does'nt affect data and can be removed later. 
        conf = []
        nonconf = []
        for seq, target in sequential_data:
            if target[-1] == 0:
                nonconf.append([seq, target])
            elif target[-1] == 1:
                conf.append([seq, target])

        random.shuffle(conf)
        random.shuffle(nonconf)

        print("Conflict: %d" % len(conf))
        print("Not conflict: %d" % len(nonconf))
        print("#############\n")

        #if count == 0:
        #    sequential_data = conf + nonconf + conf + conf
        #else:
        sequential_data = conf + nonconf  
        random.shuffle(sequential_data)

        X = []
        y = []

        #From sequence create an X and y.
        for seq, target in sequential_data:
            X.append(seq)
            y.append(target)


        if count == 0:
            train_x, train_y = X, y
        elif count == 1:
            val_x, val_y = X, y
        elif count == 2:
            test_x, test_y = X, y
        
        sequential_data = []
                                 
    return np.array(train_x), np.array(train_y), np.array(val_x), np.array(val_y), np.array(test_x), np.array(test_y), scaler

"""Make the data balanced. ie: as many conflict events as non-conflict events."""
def balanced_data(X_train, y_train):
    y_train_last = y_train[:,-1]
    conflict_seq = np.argwhere(y_train_last == 1)
    nonconflict_seq = np.argwhere(y_train_last == 0)
    
    size = min(len(conflict_seq), len(nonconflict_seq))

    conflict_seq = conflict_seq[:size]
    nonconflict_seq = nonconflict_seq[:size]


    seq = np.append(conflict_seq, nonconflict_seq)
    random.shuffle(seq)
    
    X_train_all = X_train[seq]
    y_train_all = y_train[seq]
    
    return X_train_all, y_train_all

""" "main function" for all preprocessing steps"""
def preprocess(LAG, STEP, CONFLICT, save_data=True, check_for_data=True, africa=False):
    data = concat_data()
    train, val, test = split_data(data)
    
    #Check if data already exists. 
    if check_for_data == True:
        check = check_path(LAG,STEP,CONFLICT,africa)

        #Load if there is a path.
        if check == True:
            print("Load data.\n")
            X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test = load_samples(LAG,STEP,CONFLICT,africa)
            scaler = load_scaler(LAG,STEP,CONFLICT,africa)
        #Otherwise create new data.
        else:
            X_train_all, y_train_all, X_val, y_val,  X_test, y_test, scaler = create_sample(lag=LAG, step=STEP, conflict=CONFLICT,africa=africa)
            X_train, y_train = balanced_data(X_train_all, y_train_all)
    #Otherwise create new data.    
    else:
        X_train_all, y_train_all, X_val, y_val,  X_test, y_test, scaler = create_sample(lag=LAG, step=STEP, conflict=CONFLICT,africa=africa)
        X_train, y_train = balanced_data(X_train_all, y_train_all)
    
    #Could'nt find any better solution than this for saving.
    if save_data == True:
        if africa == True:
            path = f"data/processed/model/sample/X_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
            np.save(path,X_train)
            path = f"data/processed/model/sample/y_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
            np.save(path,y_train)
            path = f"data/processed/model/sample/X_train_all_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
            np.save(path,X_train_all)
            path = f"data/processed/model/sample/y_train_all_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
            np.save(path,y_train_all)
            path = f"data/processed/model/sample/X_val_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
            np.save(path,X_val)
            path = f"data/processed/model/sample/y_val_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
            np.save(path,y_val)
            path = f"data/processed/model/sample/X_test_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
            np.save(path,X_test)
            path = f"data/processed/model/sample/y_test_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
            np.save(path,y_test)

            scaler_path = f"data/processed/model/sample/scaler_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.gz"
            joblib.dump(scaler, scaler_path)
        else:
            path = f"data/processed/model/sample/X_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
            np.save(path,X_train)
            path = f"data/processed/model/sample/y_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
            np.save(path,y_train)
            path = f"data/processed/model/sample/X_train_all_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
            np.save(path,X_train_all)
            path = f"data/processed/model/sample/y_train_all_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
            np.save(path,y_train_all)
            path = f"data/processed/model/sample/X_val_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
            np.save(path,X_val)
            path = f"data/processed/model/sample/y_val_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
            np.save(path,y_val)
            path = f"data/processed/model/sample/X_test_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
            np.save(path,X_test)
            path = f"data/processed/model/sample/y_test_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
            np.save(path,y_test)

            scaler_path = f"data/processed/model/sample/scaler_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.gz"
            joblib.dump(scaler, scaler_path)
    
    
    return X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler