import numpy as np
from data_preprocessing import preprocess
from model import LSTM_model
from sklearn import metrics
from serealize import load_model_from_file, save_model

def column_selector():
    
    #Parameters
    lag = 12
    step = 6
    conflict = 1
    africa = False
    batch_size = 128
    iterations = 20
    
    logs = 0
    rocs = 0
    prs = 0
    briers = 0
    
    columns = ['country_id', 'type_1', 'type_2', 'type_3', 'deaths', 'nb_type_1',
        'nb_type_2', 'nb_type_3', 'nb_deaths', 'e_wb_pop', 'e_miferrat',
        'e_peaveduc', 'e_area', 'e_pelifeex', 'e_peinfmor', 'v2x_clphy',
        'v2x_rule', 'e_regiongeo', 'e_migdpgro', 'e_migdppc', 'e_cow_exports',
        'e_cow_imports', 'protests', 'riots', 'v2x_regime_1.0',
        'v2x_regime_2.0', 'v2x_regime_3.0', 'e_pt_coup_1.0', 'e_pt_coup_2.0']
    
    cols = [1,2,3,4,9,12,17,18,19,20,21,24,25,26,27,28]
    for r in range(iterations):

        print("Iteration %d." % r)
        X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=lag, STEP=step, CONFLICT=conflict)

        X_train = X_train[:,:,cols]
        X_val = X_val[:,:,cols]
        X_train_all = X_train_all[:,:,cols]

        save_model(lag,step,conflict,africa,batch_size,cols)
        model = load_model_from_file(step,lag, conflict, africa)

        remainder_val = X_val.shape[0]%batch_size

        y_pred = model.predict(X_val[remainder_val:])
        y_pred = np.float64(y_pred)

        log = metrics.log_loss(y_val[remainder_val:,-1], y_pred[:,-1])
        roc = metrics.roc_auc_score(y_val[remainder_val:,-1], y_pred[:,-1])
        pr = metrics.average_precision_score(y_val[remainder_val:,-1],y_pred[:,-1])
        brier = metrics.brier_score_loss(y_val[remainder_val:,-1], y_pred[:,-1])
        
        print(log)
        print(logs)
        logs += log
        print(logs)
        rocs += roc
        prs += pr
        briers += brier
        
    logs = logs/iterations
    rocs = rocs/iterations
    prs = prs/iterations
    briers = briers/iterations
    
    return logs,rocs,prs,briers