from model import LSTM_model, run_last_month, avg_model, avg_sample_model, run_rf, Simple_NN, run_logit
from data_preprocessing import preprocess


### PARAMETERS #####
LAG = 12
STEP = 6
CONFLICT = 1

EPOCHS = 50
BATCH_SIZE = 128

### DATA ######

print("Process data.\n")
X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=LAG, STEP=STEP, CONFLICT=CONFLICT, check_for_data=True)
print("Data done.\n")
    
    
#y_train = y_train[:,-1]
#y_train_all = y_train_all[:,-1]
#y_val = y_val[:,-1]
#y_test = y_test[:,-1]   

    
#X_train = X_train[:,:,[1,2,3,4,5,6,7,8,9,12,18,19,22,23,24,25,26,27,28]]
#X_val = X_val[:,:,[1,2,3,4,5,6,7,8,9,12,18,19,22,23,24,25,26,27,28]]
#X_train_all = X_train_all[:,:,[1,2,3,4,5,6,7,8,9,12,18,19,22,23,24,25,26,27,28]]
#### MODELS ######    
    
print("Running LSTM.")
model = LSTM_model(X_train, y_train, X_val, y_val, epochs=EPOCHS, batch_size=BATCH_SIZE)
print("Model done.\n")

print("Running 'last month'-model.")
right, wrong, y_hat, log, roc, pr, brier = run_last_month(X=X_val, y_val=y_val, conflict=CONFLICT, scaler=scaler)
print("Model done.\n")

print("Running average-model.")
log, roc, pr, brier, y_hat = avg_model(X_train_all, y_train_all, X_val, y_val)
print("Model done.\n")

print("Running sample-avg-model.")
log, roc, pr, brier, y_hat = avg_sample_model(X_val,y_val, LAG, scaler)
print("Model done.\n")

print("Running logit.")
model, log, roc, pr, brier, y_hat = run_logit(X_train, y_train, X_val, y_val)
print(model.coef_)
print("Model done.\n")

print("Running Random Forest.")
model, log, roc, pr, brier, y_hat = run_rf(X_train, y_train, X_val, y_val)
print("Model done.\n")

#print("Running Simple NN.")
#model = Simple_NN(X_train_all, y_train_all, X_val, y_val, epochs=EPOCHS, batch_size=BATCH_SIZE)
#print("Model done.\n")
