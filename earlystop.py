import tensorflow as tf

#Class for an earlystopping that stops directly when a metric values is achieved. 
class EarlyStoppingValAUPR(tf.keras.callbacks.Callback):
    def __init__(self, monitor='val_AUPR', value=0.91, verbose=1):
        super(tf.keras.callbacks.Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn("Early stopping requires %s available!" % self.monitor, RuntimeWarning)

        if current > self.value:
            if self.verbose > 0:
                print("Epoch %05d: early stopping THR" % epoch)
            self.model.stop_training = True