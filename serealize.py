from model import LSTM_model
from data_preprocessing import preprocess
from tensorflow.keras.models import load_model

#Train and save model
def save_model(lag,step,conflict,africa,batch_size=128,learning_rate=0.001,columns=[1,2,3,4]):
    
    #Parameters
    LAG = lag
    STEP = step
    CONFLICT = conflict
    
    LEARNING_RATE = learning_rate
    EPOCHS = 200
    BATCH_SIZE = batch_size
    
    
    
    #Load data
    print("Process data.\n")
    X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test, scaler = preprocess(LAG=LAG, STEP=step, CONFLICT=CONFLICT, africa=africa)
    
    X_train = X_train[:,:,columns]
    X_val = X_val[:,:,columns]
    X_train_all = X_train_all[:,:,columns]
    X_test = X_test[:,:,columns]
    print("Data done.\n")

    #Run model.
    print("Running LSTM.")
    model = LSTM_model(X_train, y_train, X_val, y_val, epochs=EPOCHS, batch_size=BATCH_SIZE, learning_rate=LEARNING_RATE)
    print("Model done.\n")
    
    #Save the model
    if africa == True:
        path = f"models/model_step_{STEP}_lag_{LAG}_conflict_{CONFLICT}_africa.h5"
    else:
        path = f"models/model_step_{STEP}_lag_{LAG}_conflict_{CONFLICT}.h5"
    model.save(path)
    
#Load a pre-saved model.    
def load_model_from_file(step, lag, conflict, africa=False):
    if africa == True:
        path = f"models/model_step_{step}_lag_{lag}_conflict_{conflict}_africa.h5"
    else: 
        path = f"models/model_step_{step}_lag_{lag}_conflict_{conflict}.h5"
    model = load_model(path)
    return model