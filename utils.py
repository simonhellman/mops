import pandas as pd
import numpy as np
from datetime import datetime
from dateutil import relativedelta
from pathlib import Path
import joblib

def date_to_month(column, format):
        times = column
        times = times.apply(lambda x: datetime.strptime(x, format))
        t = datetime(1989, 1, 1)
        delta_time = times.apply(lambda x: relativedelta.relativedelta(x,t))
        delta_months = delta_time.apply(lambda x: x.years * 12 + x.months)
        return delta_months
    
#Sort with respect to month
#Returns sorted dataframe
def sort_by_date(data, column):
    sorted_data = data.sort_values(by=column)
    return sorted_data

#Convert the dates into months since 1989-01-01.
#NEED TO LOOK AT DISTRIBUTION OF EVENT TIMES! Crucial in the analysis.
# Returns dataframe
def convert_to_months(data, columns, format):
    for d in columns:
        data[d] = date_to_month(data[d], format=format)        
    return data

def data_clean_dates(data, columns, format):
    data = convert_to_months(data, columns, format)
    
    # Remove datapoints from after 2018. Only Syria has data from 2019 for example.
    data = data[data[columns[0]] < 360]
    
    sorted_data = sort_by_date(data, columns[0])
    return sorted_data

def check_path(LAG,STEP,CONFLICT,africa):
    
    print("Check if samples exists.\n")
    if africa == True:
        path = f"data/processed/model/sample/X_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
    else:
        path = f"data/processed/model/sample/X_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
    
    my_file = Path(path)
    if my_file.is_file():
        print("Path exists.")
        return True
    else:
        print("Path does not exist.")
    print("Check done.\n")
    return False

def check_model_path(LAG,STEP,CONFLICT,africa):
    
    print("Check if samples exists.\n")
    if africa == True:
        path = f"models/model_step_{STEP}_lag_{LAG}_conflict_{CONFLICT}_africa.h5"
    else:
        path = f"models/model_step_{STEP}_lag_{LAG}_conflict_{CONFLICT}.h5"
    
    my_file = Path(path)
    if my_file.is_file():
        print("Path exists.")
        return True
    else:
        print("Path does not exist.")
    print("Check done.\n")
    return False





    
def load_samples(LAG, STEP, CONFLICT, africa):
    if africa == True:
        path = f"data/processed/model/sample/X_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
        X_train = np.load(path)
        path = f"data/processed/model/sample/y_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
        y_train = np.load(path)
        path = f"data/processed/model/sample/X_train_all_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
        X_train_all= np.load(path)
        path = f"data/processed/model/sample/y_train_all_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
        y_train_all = np.load(path)
        path = f"data/processed/model/sample/X_val_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
        X_val = np.load(path)
        path = f"data/processed/model/sample/y_val_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
        y_val = np.load(path)
        path = f"data/processed/model/sample/X_test_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
        X_test = np.load(path)
        path = f"data/processed/model/sample/y_test_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.npy"
        y_test = np.load(path)
    else:
        path = f"data/processed/model/sample/X_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
        X_train = np.load(path)
        path = f"data/processed/model/sample/y_train_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
        y_train = np.load(path)
        path = f"data/processed/model/sample/X_train_all_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
        X_train_all= np.load(path)
        path = f"data/processed/model/sample/y_train_all_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
        y_train_all = np.load(path)
        path = f"data/processed/model/sample/X_val_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
        X_val = np.load(path)
        path = f"data/processed/model/sample/y_val_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
        y_val = np.load(path)
        path = f"data/processed/model/sample/X_test_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
        X_test = np.load(path)
        path = f"data/processed/model/sample/y_test_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.npy"
        y_test = np.load(path)
    return X_train, y_train, X_train_all, y_train_all, X_val, y_val, X_test, y_test
    
def load_scaler(LAG, STEP, CONFLICT, africa):
    if africa == True:
        scaler = joblib.load(f"data/processed/model/sample/scaler_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}_africa.gz")
    else:
        scaler = joblib.load(f"data/processed/model/sample/scaler_LAG_{LAG}_step_{STEP}_conflict_{CONFLICT}.gz")
    return scaler